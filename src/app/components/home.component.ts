import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: '../views/home.html'
})
export class HomeComponent implements OnInit {
  public titulo: string;
  constructor() {
    this.titulo = 'Página principal';
  }

  ngOnInit() {
    console.log('Se ha cargado HomeComponent');
  }

  toggleTitle() {
    console.log('Le has dado clic al botón');
    // jQuery('.title').slideToggle();
    $('.title').slideToggle();
  }
}
