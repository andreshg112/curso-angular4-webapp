import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../models/producto';

@Component({
  selector: 'app-productos-list',
  templateUrl: '../views/productos-list.html',
  providers: [ProductoService]
})
export class ProductosListComponent implements OnInit {
  public confirmado: any = null;
  public titulo: string;
  public productos: Producto[];
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productosService: ProductoService
  ) {
    this.titulo = 'Listado de productos';
  }

  ngOnInit() {
    console.log('Se ha cargado ProductosListComponent');
    this.getProductos();
  }

  private getProductos() {
    this._productosService.getProductos().subscribe(result => {
      if (result.code != 200) {
        console.log(result);
      } else {
        this.productos = result.data;
      }
    }, error => {
      console.log(<any>error);
    });
  }

  borrarConfirm(id) {
    this.confirmado = id;
  }

  cancelarConfirm() {
    this.confirmado = null;
  }

  onDeleteProducto(id) {
    this._productosService.deleteProducto(id).subscribe(
      response => {
        if (response.code == 200) {
          this.getProductos();
        } else {
          console.log(response);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
}
